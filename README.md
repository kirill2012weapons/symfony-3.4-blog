Symfony Standard Edition
========================

**WARNING**: This distribution does not support Symfony 4. See the
[Installing & Setting up the Symfony Framework][15] page to find a replacement
that fits you best.

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * An AppBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev env) - Adds code generation
    capabilities

  * [**WebServerBundle**][14] (in dev env) - Adds commands for running applications
    using the PHP built-in web server

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!

[1]:  https://symfony.com/doc/3.4/setup.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.4/doctrine.html
[8]:  https://symfony.com/doc/3.4/templating.html
[9]:  https://symfony.com/doc/3.4/security.html
[10]: https://symfony.com/doc/3.4/email.html
[11]: https://symfony.com/doc/3.4/logging.html
[13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
[14]: https://symfony.com/doc/current/setup/built_in_web_server.html
[15]: https://symfony.com/doc/current/setup.html



#### Development server

Run `npm start` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change 
any of the source files.
* Use the `npm run start:prod` command for a production dev server.

#### Build

Run `npm run build` to build the project. The build artifacts will be stored in the `public/` directory.
* Use the `npm run build:prod` command for a production build.
* Use the `npm run build:sourcemap` command for a build with sourcemap.
* Use the `npm run build:prod:sourcemap` command for a production build with sourcemap.

#### Watch

Run `npm run watch` to watch the project. The build artifacts will be stored in the `public/` directory.
* Use the `npm run watch:prod` command for a production watch.
* Use the `npm run watch:sourcemap` command for a watch with sourcemap.
* Use the `npm run watch:prod:sourcemap` command for a production watch with sourcemap.

#### Linters

Run `npm run lint` to find problematic patterns or code that doesn’t adhere to certain style guidelines in the 
project. You can also use `npm run lint-fix` to fix them.


#### Code scaffolding

* Anchor - https://gitlab.com/izumi-it-company/webpack-iic/snippets/1874134
* LazyLoad - https://gitlab.com/izumi-it-company/webpack-iic/snippets/1873799

#### Here's an example how to use
<hr/>

##### How to configure html tracking

By default, html files are tracking in the `src` directory.

**webpack.config.js**
```javascript
const indexChunk = getHtmlWebpackPlugins( './src', 'html' );
const allHtmlChunk = [ ...indexChunk ];
```

For example, add tracking of html files in the `src/work` directory.

**webpack.config.js**
```javascript
const indexChunk = getHtmlWebpackPlugins( './src', 'html' );
const workChunk = getHtmlWebpackPlugins( './src/work', 'html' );
const allHtmlChunk = [ ...indexChunk, ...workChunk ];
``` 

##### Metatags description and keywords are in webpack.config.js in the variable DEFAULTS
```html
<meta name="description" content="<%= htmlWebpackPlugin.options.description %>">
<meta name="keywords" content="<%= htmlWebpackPlugin.options.keywords %>">
```
**webpack.config.js**
```javascript
const DEFAULTS = { description: 'description', keywords: 'keywords' };
```

##### How to use some library

Use commonJs or ES module

* CommonJs  
https://requirejs.org/docs/commonjs.html
* ES module  
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import  
https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export

For example, download *Swiper* `npm i swiper` and use them in js file.

```javascript
import Swiper from 'swiper';
const mySwiper = new Swiper('.swiper-container', { /* ... */ });
```

##### How to open development server on remote device

Paste `YOUR_LOCAL_IP` address to host property as string. Navigate to `http://YOUR_LOCAL_IP:8080/`  
Run `npm run ip` to see your local addresses

**webpack.config**
```javascript
/* devServer */
config.devServer = {
 contentBase: path.resolve(__dirname, './src'),
 host: 'YOUR_LOCAL_IP',
 port: 8080,
 stats: 'minimal',
 overlay: true,
 hot: IS_HOST,
 watchContentBase: true,
 open: true,
};
```
