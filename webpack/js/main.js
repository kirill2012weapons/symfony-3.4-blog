import $ from 'jquery';
import sal from 'sal.js';

$(document).ready(function () {
	mobileMenu();
	sal();
	headerScroll();
	anchorLink();
	adminDropdownLink();
});

function mobileMenu() {
	$('.mob-menu__open').click(function (e) {
		e.preventDefault();
		$(this).toggleClass('mob-menu__close');
		$('.main-nav:not(.footer-nav)').toggleClass('main-nav_active');
		$('.social__list').toggleClass('social__list_hidden');
		$('body, html').toggleClass('overflow-hidden');
	});
}

function headerScroll() {
	var $headerHeight = $('.header').height(),
			$toTop 				= $('.to-top');
	$(window).scroll(function () {
		if (window.pageYOffset > $headerHeight) {
			$('.header').addClass('header_visible');
			$toTop.addClass('to-top_visible');
		}
		else{
			$toTop.removeClass('to-top_visible');
		}
	});
}

function anchorLink() {

	const $anchor = $('.anchor');
	const $footer = $('#footer');

	$(document).scroll(() => {
		if ( ($footer.get(0).getBoundingClientRect().top - window.innerHeight) < 0 ) {
			$anchor.addClass('stick');
		} else {
			$anchor.removeClass('stick');
		}
	});

	$anchor.click(function (e) {
		e.preventDefault();
		$('body,html').animate({scrollTop: 0}, 1000);
	});
}

function adminDropdownLink(){
	$('li.dropdown-profile > a').click(function (e) {
		e.preventDefault();

		$(this).next().toggleClass('active');
	});
}
