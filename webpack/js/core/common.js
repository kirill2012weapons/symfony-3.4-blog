import { Emitter } from './emitter';

const emitter = new Emitter();

/* Singleton */
export class Singleton {

	constructor( className ) {
		if ( !className._instance ) className._instance = this;
		else return className._instance;
	}

}

/* State container */
export class State {

	constructor( container, nextClassState ) {
		this.container = container;
		this._nextClassState = nextClassState;
	}

	next() {
		return new this._nextClassState( this.container );
	}

}

/* Bind decorator */
export function Bind( methodName ) {
	return function ( classPrototype, propertyName, descriptor ) {
		if ( descriptor.initializer ) {
			classPrototype[ '_' + propertyName ] = descriptor.initializer();
		}

		delete descriptor.writable;
		delete descriptor.initializer;

		descriptor.get = function() {
			return this[ '_' + propertyName ];
		};
		descriptor.set = function( value ) {
			this[ '_' + propertyName ] = value;
			this[ methodName ]();
		};
	};
}

/* Listener */

export function Listener( $target, eventName ) {
	return function ( classPrototype, propertyName ) {
		// emit with initiate instance
		emitter.on( classPrototype.constructor.name, (context) => {
			// set listener on target
			$target.addEventListener( eventName, (event) => context[ propertyName ]( event ) );
			// remove listener after initiate
			emitter.off( classPrototype.constructor.name );
		});
	};
}

/* OnView - IntersectionObserver */
function onViewIO( classInstance ) {
	return new IntersectionObserver( (entries, observer) => {
		entries.forEach( entry => {

			if ( entry.isIntersecting ) {
				classInstance.onView();
				observer.disconnect();
			}
		});
	});
}

/* Component decorator */
export function Initiate( options = {} ) {
	return function( classPrototype ) {
		return function() {

			const classInstance = new classPrototype();
			emitter.emit( classPrototype.name, classInstance );
			// onView init
			options.onView && onViewIO( classInstance ).observe( options.onView );

		};
	};
}
