<?php

namespace ArtistBundle\Form;


use ArtistBundle\Entity\Mail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SendMailType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',   TextType::class, [
                'label'     => 'Name',
            ])
            ->add('phone',  TextType::class, [
                'label'     => 'Phone',
            ])
            ->add('email',  EmailType::class, [
                'label'     => 'Email',
            ])
            ->add('text',   TextType::class, [
                'label'     => 'Text',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mail::class,
        ]);
    }

    public function getName()
    {
        return 'send_mail_form';
    }

}