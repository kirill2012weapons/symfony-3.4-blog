<?php

namespace ArtistBundle\Controller;

use ArtistAdminBundle\Entity\Blog\Information;
use ArtistAdminBundle\Entity\Blog\Profile;
use ArtistAdminBundle\Repository\InformationRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function index()
    {
        /**
         * @var $em                 EntityManager
         * @var $infoRepository     InformationRepository
         * @var $profileInfo        Profile
         */
        $em = $this->getDoctrine()->getManager();
        $infoRepository = $em->getRepository(Information::class);
        $profileRepository = $em->getRepository(Profile::class);

        $blogPosts          = $infoRepository->getAllInformationByCountAsArray(5);
        $profileInfo        = $profileRepository->getFirstOrNewProfileResult();

        return $this->render('@view.art/Index/index.html.twig', [
            'blogPosts'             => $blogPosts,
            'profileInfo'           => $profileInfo,
        ]);
    }
}
