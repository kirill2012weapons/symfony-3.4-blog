<?php

namespace ArtistBundle\Controller;

use ArtistAdminBundle\Entity\Options\Option;
use ArtistAdminBundle\Form\Options\OptionSMTP;
use ArtistBundle\Entity\Mail;
use ArtistBundle\Form\SendMailType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        /**
         * @var $em             EntityManager
         */
        $isSend = false;

        $mail = new Mail();
        $mailForm = $this->createForm(SendMailType::class, $mail);
        $mailForm->handleRequest($request);

        if ($mailForm->isSubmitted() && $mailForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            try {

                $em->persist($mail);
                $isSend = true;

                $mailer = $this->get('mail.sender');

                $optRep = $em->getRepository(Option::class);

                /**
                 * OPTIONS SMTP
                 */
                $smtpClass = new OptionSMTP();
                $smtpClass->setUserHost(                $optRep->getOptionBySlug(Option::SMTP_MAILER_HOST       ) );
                $smtpClass->setUserUsername(            $optRep->getOptionBySlug(Option::SMTP_MAILER_USER       ) );
                $smtpClass->setUserPass(                $optRep->getOptionBySlug(Option::SMTP_MAILER_PASSWORD   ) );
                $smtpClass->setUserPort(                $optRep->getOptionBySlug(Option::SMTP_MAILER_PORT       ) );
                $smtpClass->setUserEmail(               $optRep->getOptionBySlug(Option::SMTP_MAILER_EMAIL      ) );

                $message = \Swift_Message::newInstance()
                    ->setSubject('ART FORMS')
                    ->setFrom( $smtpClass->getUserUsername() ? $smtpClass->getUserUsername()->getValue() : '' )
                    ->setTo( $smtpClass->getUserEmail() ? $smtpClass->getUserEmail()->getValue() : '' )
                    ->setContentType('text/html')
                    ->setBody(
                        $this->renderView('@view.art_admin/mail_templating/form_send_mail_to_admin.html.twig', [
                            'email'         => $mail->getEmail(),
                            'name'          => $mail->getName(),
                            'phone'         => $mail->getPhone(),
                            'text'          => $mail->getText(),
                        ])
                    )
                ;

                $em->flush();
                $mailer->send($message);

                $this->addFlash('mail', 'Mail is send.');
                return $this->redirectToRoute('artist_contact_index');

            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                return $this->redirectToRoute('artist_index');
            }

        }

        return $this->render('@view.art/Contact/index.html.twig', [
            'mailForm'      => $mailForm->createView(),
            'isSend'        => $isSend,
        ]);
    }
}
