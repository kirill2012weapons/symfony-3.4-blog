<?php

namespace ArtistBundle\Controller;

use ArtistAdminBundle\Entity\Blog\Profile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProfileController extends Controller
{
    public function index()
    {
        /**
         * @var $em                 EntityManager
         * @var $profileInfo        Profile
         */
        $em = $this->getDoctrine()->getManager();
        $profileRepository = $em->getRepository(Profile::class);

        $profileInfo        = $profileRepository->getFirstOrNewProfileResult();

        return $this->render('@view.art/Profile/index.html.twig', [
            'profileInfo'       => $profileInfo,
        ]);
    }
}
