<?php

namespace ArtistBundle\Controller;

use ArtistAdminBundle\Entity\Blog\Information;
use ArtistAdminBundle\Repository\InformationRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class InformationController extends Controller
{
    public function index()
    {
        /**
         * @var $em             EntityManager
         * @var $infoRepository InformationRepository
         */
        $em = $this->getDoctrine()->getManager();
        $infoRepository = $em->getRepository(Information::class);

        $blogPosts = $infoRepository->getAllInformationAllAsArray();

        return $this->render('@view.art/Information/index.html.twig', [
            'blogPosts'     => $blogPosts,
        ]);
    }

    public function single($slug, Request $request)
    {
        /**
         * @var $em             EntityManager
         * @var $infoRep        InformationRepository
         * @var $currentPost    Information
         */
        $em = $this->getDoctrine()->getManager();
        $infoRep = $em->getRepository(Information::class);

        try {
            $currentPost = $infoRep->findByTitle(rawurldecode($slug));
            if (!$currentPost) return $this->redirectToRoute('artist_information_index');
        } catch (\Exception $exception) {
            return $this->redirectToRoute('artist_information_index');
        }

        return $this->render('@view.art/Information/single.html.twig', [
            'currentPost'               => $currentPost,
            'links'                     => $this->get('info.link_manager')->getRelatedLinks($currentPost)
        ]);
    }

}
