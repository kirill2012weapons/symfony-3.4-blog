<?php

namespace ArtistBundle\Controller;

use ArtistAdminBundle\Entity\Gallery\Gallery;
use ArtistAdminBundle\Repository\GalleryRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GalleryController extends Controller
{
    public function index($year, Request $request)
    {
        /**
         * @var $em                     EntityManager
         * @var $galleryRepository      GalleryRepository
         */
        $em = $this->getDoctrine()->getManager();
        $galleryRepository = $em->getRepository(Gallery::class);

        $posts = $galleryRepository->getPostByYearAsArray($year);
        $postYears = $galleryRepository->getListOfPostGallaryAsArrayGroupByYear();

        if (is_null($posts) || empty($posts)) throw $this->createNotFoundException('Gallery post not found.');

        return $this->render('@view.art/Gallery/index.html.twig', [
            'posts'                         => $posts,
            'postYears'                     => $postYears,
            'yearCurrent'                   => $year,
        ]);
    }
}
