<?php

namespace AppBundle\Controller;

use ArtistAdminBundle\Entity\User;
use GeoIp2\Database\Reader;
use MetzWeb\Instagram\Instagram;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/test", name="test")
     */
    public function indexAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {



        $user = new User();
        $user->setRoles(User::ROLE_ADMIN);
        $user->setName('Art');
        $user->setSurname('Art');
        $user->setEmail('izumi@izumi-it-company.com');
        $user->setUsername('Art');
        $password = $passwordEncoder->encodePassword($user, 'admin');
        $user->setPassword($password);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $var = [];

//        $instagram = new Instagram('01806003921b426490b8c09aa50b9e70');
//
//        $instagram->setAccessToken('3298205723.1677ed0.90c56d231cd54a71a80bf066106be7d8');
//
//
//        $var = $instagram->getUserMedia();


//        $api = new \Instagram\Api();
//        $api->setUserId('3298205723');
//        $api->setAccessToken('91b1faf4cf7b45b690e212913ab0e62f');
//
//        $var = $api->getFeed('prostoalya1');



        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);

    }
}
