<?php

namespace ArtistAdminBundle\Validator\Constraints;


use ArtistAdminBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ContainsIsPasswordEqualValidator extends ConstraintValidator
{

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var UserPasswordEncoder
     */
    protected $decoder;

    /**
     * @var User
     */
    protected $user;

    public function __construct(EntityManager $entityManager, UserPasswordEncoder $decoder, $tokenStorage)
    {
        $this->em           = $entityManager;
        $this->decoder      = $decoder;
        $this->user         = $tokenStorage->getToken()->getUser();
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ContainsIsPasswordEqual) {
            throw new UnexpectedTypeException($constraint, ContainsIsPasswordEqual::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!$this->decoder->isPasswordValid( $this->user, $value )) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
        return;

    }

}