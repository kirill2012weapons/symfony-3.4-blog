<?php

namespace ArtistAdminBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsIsPasswordEqual extends Constraint
{

    public $message = 'The password is not equal old one.';

    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }

}