<?php

namespace ArtistAdminBundle\Controller;


use ArtistAdminBundle\Entity\Uploading\File;
use ArtistAdminBundle\Entity\User;
use ArtistAdminBundle\Form\Security\LoginType;
use ArtistAdminBundle\Form\Security\UserEmailType;
use ArtistAdminBundle\Form\Security\UserInformationType;
use ArtistAdminBundle\Form\Security\UserPasswordType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {

        $formLogin = $this->createForm(LoginType::class);

        $formLogin->handleRequest($request);

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        if (!empty($lastUsername)) $this->get('alert')->error($lastUsername);

        return $this->render('@view.art_admin/Security/login.html.twig', [
            'error'             => $error,
            'lastUserName'      => $lastUsername,
            'formLogin'         => $formLogin->createView(),
        ]);
    }

    public function logout(Request $request){
        $this->get('alert')->warning('User is log out.');
    }

    public function profile(Request $request)
    {

        return $this->render('@view.art_admin/Security/my_profile.html.twig', [
        ]);

    }

    public function changePassword(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {

        /**
         * @var $user               User
         * @var $em                 EntityManager
         */

        $user = $this->getUser();

        $changePasswordForm = $this->createForm(UserPasswordType::class, $user);
        $changePasswordForm->handleRequest($request);


        if ($changePasswordForm->isSubmitted() && $changePasswordForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $password = $passwordEncoder->encodePassword($user, $changePasswordForm->get('newPassword')->getData());
            $user->setPassword($password);

            try {
                $em->flush();
                $this->get('alert')->success('Password has been changed');
                return $this->redirectToRoute('artist_admin_logout');
            } catch ( \Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
            }

        }

        return $this->render('@view.art_admin/Security/change_pawword.html.twig', [
            'changePasswordForm'           => $changePasswordForm->createView(),
        ]);

    }

    public function changeEmail(Request $request)
    {

        /**
         * @var $user               User
         * @var $em                 EntityManager
         */

        $user = $this->getUser();

        $changeEmailForm = $this->createForm(UserEmailType::class, $user);
        $changeEmailForm->handleRequest($request);

        if ($changeEmailForm->isSubmitted() && $changeEmailForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            try {
                $em->flush();
                $this->get('alert')->success('Email has been changed');
                return $this->redirectToRoute('artist_admin_logout');
            } catch ( \Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
            }

        }

        return $this->render('@view.art_admin/Security/change_emain.html.twig', [
            'changeEmailForm'           => $changeEmailForm->createView(),
        ]);

    }

    public function profileChange(Request $request)
    {
        /**
         * @var $em                 EntityManager
         * @var $user               User
         */


        $user = $this->getUser();

        $userInformationForm = $this->createForm(UserInformationType::class, $user);

        $userInformationForm->handleRequest($request);

        if ($userInformationForm->isSubmitted() && $userInformationForm->isValid()) {

            $em = $this->getDoctrine()->getManager();

            if ($userInformationForm->get('logo')->getData() instanceof UploadedFile) {
                $file = new File();
                $file->setSlug(File::FILE_USER_LOGO);
                $file->setObjFile($userInformationForm->get('logo')->getData());
                $user->setLogo($file);
                $em->persist($file);
            }

            try {
                $em->flush();
                $this->get('alert')->success('Profile has been changed');
                return $this->redirectToRoute('artist_admin_security_my_profile');
            } catch ( \Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
            }

        }

        return $this->render('@view.art_admin/Security/my_profile_change.html.twig', [
            'userInformationForm'           => $userInformationForm->createView(),
        ]);

    }

    public function forgotPassword(Request $request)
    {

        return $this->render('@view.art_admin/Security/forgot_password.html.twig', [
        ]);

    }

}
