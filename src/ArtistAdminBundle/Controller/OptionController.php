<?php

namespace ArtistAdminBundle\Controller;


use ArtistAdminBundle\Entity\Options\Option;
use ArtistAdminBundle\Form\Options\OptionFacebook;
use ArtistAdminBundle\Form\Options\OptionFacebookType;
use ArtistAdminBundle\Form\Options\OptionInstagram;
use ArtistAdminBundle\Form\Options\OptionInstagramType;
use ArtistAdminBundle\Form\Options\OptionSMTP;
use ArtistAdminBundle\Form\Options\OptionSMTPType;
use ArtistAdminBundle\Repository\OptionRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class OptionController extends Controller
{

    public function smtpOptions(Request $request)
    {
        /**
         * @var $em                 EntityManager
         * @var $optRep             OptionRepository
         */
        $em = $this->getDoctrine()->getManager();
        $optRep = $em->getRepository(Option::class);

        /**
         * OPTIONS SMTP
         */
        $smtpClass = new OptionSMTP();
        $smtpClass->setUserHost(                $optRep->getOptionBySlug(Option::SMTP_MAILER_HOST) );
        $smtpClass->setUserUsername(            $optRep->getOptionBySlug(Option::SMTP_MAILER_USER) );
        $smtpClass->setUserPass(                $optRep->getOptionBySlug(Option::SMTP_MAILER_PASSWORD) );
        $smtpClass->setUserPort(                $optRep->getOptionBySlug(Option::SMTP_MAILER_PORT) );
        $smtpClass->setUserEmail(               $optRep->getOptionBySlug(Option::SMTP_MAILER_EMAIL) );
        $smtpForm = $this->createForm(OptionSMTPType::class, $smtpClass);
        $smtpForm->handleRequest($request);
        if ($smtpForm->isSubmitted() && $smtpForm->isValid()) {
            $em->persist($smtpClass->getUserHost());
            $em->persist($smtpClass->getUserUsername());
            $em->persist($smtpClass->getUserPass());
            $em->persist($smtpClass->getUserPort());
            $em->persist($smtpClass->getUserEmail());
            try {
                $em->flush();
                $this->get('alert')->success('Option update!');
                return $this->redirectToRoute('artist_admin_options_smtp');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
                return $this->redirectToRoute('artist_admin_options_smtp');
            }
        }

        return $this->render('@view.art_admin/Options/smtp.html.twig', [
            'smtpForm'      => $smtpForm->createView(),
        ]);
    }

    public function instagramOptions(Request $request)
    {
        /**
         * @var $em                 EntityManager
         * @var $optRep             OptionRepository
         */
        $em = $this->getDoctrine()->getManager();
        $optRep = $em->getRepository(Option::class);

        /**
         * OPTIONS Instagram
         */
        $instaClass = new OptionInstagram();
        $instaClass->setUserUrl(    $optRep->getOptionBySlug(Option::INSTAGRAM_USER_URL) );
        $instagramForm = $this->createForm(OptionInstagramType::class, $instaClass);
        $instagramForm->handleRequest($request);
        if ($instagramForm->isSubmitted() && $instagramForm->isValid()) {
            $em->persist($instaClass->getUserUrl());
            try {
                $em->flush();
                $this->get('alert')->success('Option update!');
                return $this->redirectToRoute('artist_admin_options_instagram');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
                return $this->redirectToRoute('artist_admin_options_instagram');
            }
        }

        return $this->render('@view.art_admin/Options/instagram.html.twig', [
            'instagramForm'      => $instagramForm->createView(),
        ]);
    }

    public function facebookOptions(Request $request)
    {
        /**
         * @var $em                 EntityManager
         * @var $optRep             OptionRepository
         */
        $em = $this->getDoctrine()->getManager();
        $optRep = $em->getRepository(Option::class);

        /**
         * OPTIONS Facebook
         */
        $faceClass = new OptionFacebook();
        $faceClass->setUserUrl(     $optRep->getOptionBySlug(Option::FACEBOOK_USER_URL) );
        $facebookForm = $this->createForm(OptionFacebookType::class, $faceClass);
        $facebookForm->handleRequest($request);
        if ($facebookForm->isSubmitted() && $facebookForm->isValid()) {
            $em->persist($faceClass->getUserUrl());
            try {
                $em->flush();
                $this->get('alert')->success('Option update!');
                return $this->redirectToRoute('artist_admin_options_facebook');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
                return $this->redirectToRoute('artist_admin_options_facebook');
            }
        }

        return $this->render('@view.art_admin/Options/facebook.html.twig', [
            'facebookForm'      => $facebookForm->createView(),
        ]);
    }

}
