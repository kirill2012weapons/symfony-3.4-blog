<?php

namespace ArtistAdminBundle\Controller;

use ArtistAdminBundle\Entity\Blog\Information;
use ArtistAdminBundle\Entity\Uploading\File;
use ArtistAdminBundle\Form\Information\BlogInformationType;
use ArtistAdminBundle\Repository\InformationRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class InformationController extends Controller
{

    public function delete($id, Request $request)
    {
        /**
         * @var $em             EntityManager
         */
        $em = $this->getDoctrine()->getManager();

        try {
            $blogPost = $em->find(Information::class, $id);
        } catch (\Exception $exception) {
            $this->get('alert')->error('Post Not found.');
            return $this->redirectToRoute('artist_admin_information_show_all');
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($blogPost);
        try {
            $em->flush();
            $this->get('alert')->success('Post has been deleted');
            return $this->redirectToRoute('artist_admin_information_show_all');
        } catch (\Doctrine\ORM\OptimisticLockException $exception) {
            $this->get('alert')->error($exception->getMessage());
            return $this->redirectToRoute('artist_admin_information_show_all');
        }

    }

    public function modify($id, Request $request)
    {
        /**
         * @var $em             EntityManager
         */
        $em = $this->getDoctrine()->getManager();

        try {
            $blogPost = $em->find(Information::class, $id);
        } catch (\Exception $exception) {
            $this->get('alert')->error('Post Not found.');
            return $this->redirectToRoute('artist_admin_information_show_all');
        }

        $modifyInfoForm = $this->createForm(BlogInformationType::class, $blogPost);
        $modifyInfoForm->handleRequest($request);

        if ($modifyInfoForm->isSubmitted() && $modifyInfoForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($modifyInfoForm->get('thumbnail')->getData() instanceof UploadedFile) {
                $file = new File();
                $file->setSlug(File::FILE_USER_BLOG_THUMBNAIL);
                $file->setObjFile($modifyInfoForm->get('thumbnail')->getData());
                $blogPost->setThumbnail($file);
                $em->persist($file);
            }
            try {
                $em->flush();
                $this->get('alert')->success('Success. The post has been changed.');
                return $this->redirectToRoute('artist_admin_information_show_all');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
                return $this->redirectToRoute('artist_admin_information_show_all');
            }

        }

        return $this->render('@view.art_admin/Information/modify.html.twig', [
            'modifyInfoForm'            => $modifyInfoForm->createView(),
            'blogPost'                  => $blogPost,
        ]);
    }

    public function showAll()
    {
        /**
         * @var $em             EntityManager
         * @var $infoRepository InformationRepository
         */
        $em = $this->getDoctrine()->getManager();
        $infoRepository = $em->getRepository(Information::class);

        $blogPosts = $infoRepository->getAllInformationAllAsArray();

        return $this->render('@view.art_admin/Information/show_all.html.twig', [
            'blogPosts'             => $blogPosts,
        ]);
    }

    public function addNew(Request $request)
    {
        /**
         * @var $em             EntityManager
         */

        $information = new Information();
        $addNewInfoForm = $this->createForm(BlogInformationType::class, $information);

        $addNewInfoForm->handleRequest($request);

        if ($addNewInfoForm->isSubmitted() && $addNewInfoForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $information->setUser($this->getUser());

            if ($addNewInfoForm->get('thumbnail')->getData() instanceof UploadedFile) {
                $file = new File();
                $file->setSlug(File::FILE_USER_BLOG_THUMBNAIL);
                $file->setObjFile($addNewInfoForm->get('thumbnail')->getData());
                $information->setThumbnail($file);
                $em->persist($file);
            }
            $em->persist($information);
            try {
                $em->flush();
                $this->get('alert')->success('Success. Add new blog-information!');
                return $this->redirectToRoute('artist_admin_information_show_all');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
                return $this->redirectToRoute('artist_admin_information_show_all');
            }

        }

        return $this->render('@view.art_admin/Information/add_new.html.twig', [
            'addNewInfoForm'            => $addNewInfoForm->createView(),
            'blogPost'                  => $information,
        ]);
    }
}
