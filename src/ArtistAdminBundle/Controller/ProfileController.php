<?php

namespace ArtistAdminBundle\Controller;

use ArtistAdminBundle\Entity\Blog\Information;
use ArtistAdminBundle\Entity\Blog\Profile;
use ArtistAdminBundle\Entity\Uploading\File;
use ArtistAdminBundle\Form\Information\BlogInformationType;
use ArtistAdminBundle\Form\Information\BlogProfileType;
use ArtistAdminBundle\Repository\InformationRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends Controller
{

    public function modify(Request $request)
    {
        /**
         * @var $em                 EntityManager
         * @var $profile            Profile
         */
        $em = $this->getDoctrine()->getManager();
        $profileRepository = $em->getRepository(Profile::class);

        $profile = $profileRepository->getFirstOrNewProfileResult();


        $profileForm = $this->createForm(BlogProfileType::class, $profile);
        $profileForm->handleRequest($request);

        if ($profileForm->isSubmitted() && $profileForm->isValid()) {

            if ($profileForm->get('thumbnail')->getData() instanceof UploadedFile) {
                $file = new File();
                $file->setSlug(File::FILE_PROFILE_USER_LOGO);
                $file->setObjFile($profileForm->get('thumbnail')->getData());
                $profile->setThumbnail($file);
                $em->persist($file);
            }

            $em->persist($profile);

            try {
                $em->flush();
                $this->get('alert')->success('Profile has been updated.');
                return $this->redirectToRoute('artist_admin_profile');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
                return $this->redirectToRoute('artist_admin_profile');
            }

        }

        return $this->render('@view.art_admin/Profile/modify.html.twig', [
            'profileForm'           => $profileForm->createView(),
            'blogPost'              => $profile
        ]);
    }


}
