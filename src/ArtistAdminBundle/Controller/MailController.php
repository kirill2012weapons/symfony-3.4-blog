<?php

namespace ArtistAdminBundle\Controller;


use ArtistAdminBundle\Repository\MailRepository;
use ArtistBundle\Entity\Mail;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MailController extends Controller
{


    public function showAll(Request $request)
    {
        /**
         * @var $em                 EntityManager
         * @var $mailRepository     MailRepository
         */
        $em = $this->getDoctrine()->getManager();
        $mailRepository = $em->getRepository(Mail::class);

        $messages = $mailRepository->getAllMailsSortedLast();

        return $this->render('@view.art_admin/Mail/show_all.html.twig', [
            'messages'          => $messages,
        ]);
    }

    public function showCurrent(Request $request, $id)
    {
        /**
         * @var $em             EntityManager
         */

        $em = $this->getDoctrine()->getManager();

        try {
            $mailCurrent = $em->find(Mail::class, $id);
        } catch (\Exception $exception) {
            throw $this->createNotFoundException('Not Found');
        }

        if (!$mailCurrent->getisShow()) {
            $mailCurrent->setIsShow(true);
            try {
                $em->flush();
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error($exception->getMessage());
                return $this->redirectToRoute('artist_admin_mail_show_all');
            }
        }

        return $this->render('@view.art_admin/Mail/show_current.html.twig', [
            'mailCurrent'           => $mailCurrent,
        ]);
    }

    public function deleteMessage($id, Request $request)
    {
        /**
         * @var $em             EntityManager
         */
        $em = $this->getDoctrine()->getManager();

        try {
            $messagePost = $em->find(Mail::class, $id);
        } catch (\Exception $exception) {
            $this->get('alert')->error('Message Not found.');
            return $this->redirectToRoute('artist_admin_mail_show_all');
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($messagePost);
        try {
            $em->flush();
            $this->get('alert')->success('Message has been deleted');
            return $this->redirectToRoute('artist_admin_mail_show_all');
        } catch (\Doctrine\ORM\OptimisticLockException $exception) {
            $this->get('alert')->error($exception->getMessage());
            return $this->redirectToRoute('artist_admin_mail_show_all');
        }
    }
}
