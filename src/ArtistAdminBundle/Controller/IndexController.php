<?php

namespace ArtistAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function index()
    {

        $insta = $this->get('instagram');

        return $this->render('@view.art_admin/Index/index.html.twig', [
            'istagramPhoto'         => $insta->getFeed(5),
            'userInfo'              => $insta->getUserInfo(),
        ]);
    }
}
