<?php

namespace ArtistAdminBundle\Controller;

use ArtistAdminBundle\Entity\Blog\Information;
use ArtistAdminBundle\Entity\Gallery\Gallery;
use ArtistAdminBundle\Entity\Uploading\File;
use ArtistAdminBundle\Form\Gallery\GalleryBlogType;
use ArtistAdminBundle\Form\Information\BlogInformationType;
use ArtistAdminBundle\Repository\GalleryRepository;
use ArtistAdminBundle\Repository\InformationRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class GalleryController extends Controller
{

    public function delete($id, Request $request)
    {

        /**
         * @var $em             EntityManager
         */
        $em = $this->getDoctrine()->getManager();

        try {
            $blogPost = $em->find(Gallery::class, $id);
        } catch (\Exception $exception) {
            $this->get('alert')->error('Post Not found.');
            return $this->redirectToRoute('artist_admin_gallery_show_all');
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($blogPost);
        try {
            $em->flush();
            $this->get('alert')->success('Post has been deleted');
            return $this->redirectToRoute('artist_admin_gallery_show_all');
        } catch (\Doctrine\ORM\OptimisticLockException $exception) {
            $this->get('alert')->error($exception->getMessage());
            return $this->redirectToRoute('artist_admin_gallery_show_all');
        }

    }

    public function modify($id, Request $request)
    {

        /**
         * @var $em             EntityManager
         */
        $em = $this->getDoctrine()->getManager();

        try {
            $galleryPost = $em->find(Gallery::class, $id);
        } catch (\Exception $exception) {
            $this->get('alert')->error($exception->getMessage());
            return $this->redirectToRoute('artist_admin_gallery_show_all');
        }

        $addNewGalleryPost = $this->createForm(GalleryBlogType::class, $galleryPost);
        $addNewGalleryPost->handleRequest($request);

        if ($addNewGalleryPost->isSubmitted() && $addNewGalleryPost->isValid()) {

            try {
                $em->flush();
                $this->get('alert')->success('The post is created.');
                return $this->redirectToRoute('artist_admin_gallery_show_all');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error('Cannot create new Gallery Post. ' . $exception->getMessage());
                return $this->redirectToRoute('artist_admin_gallery_show_all');
            }

        }

        return $this->render('@view.art_admin/Gallery/modify.html.twig', [
            'addNewGalleryPost'             => $addNewGalleryPost->createView(),
        ]);

    }

    public function showAll()
    {
        /**
         * @var $em                         EntityManager
         * @var $galleryRepository          GalleryRepository
         */
        $em = $this->getDoctrine()->getManager();
        $galleryRepository = $em->getRepository(Gallery::class);

        $galleryPosts = $galleryRepository->getAllPostsGalleryAsArray();

        return $this->render('@view.art_admin/Gallery/show_all.html.twig', [
            'galleryPosts'          => $galleryPosts,
        ]);
    }

    public function addNew(Request $request)
    {
        /**
         * @var $em             EntityManager
         */


        $galleryPost = new Gallery();

        $addNewGalleryPost = $this->createForm(GalleryBlogType::class, $galleryPost);
        $addNewGalleryPost->handleRequest($request);

        dump($galleryPost);

        if ($addNewGalleryPost->isSubmitted() && $addNewGalleryPost->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($galleryPost);

            try {
                $em->flush();
                $this->get('alert')->success('The post is created.');
                return $this->redirectToRoute('artist_admin_gallery_show_all');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->get('alert')->error('Cannot create new Gallery Post. ' . $exception->getMessage());
                return $this->redirectToRoute('artist_admin_gallery_show_all');
            }

        }

        return $this->render('@view.art_admin/Gallery/add.html.twig', [
            'addNewGalleryPost'             => $addNewGalleryPost->createView(),
        ]);
    }
}
