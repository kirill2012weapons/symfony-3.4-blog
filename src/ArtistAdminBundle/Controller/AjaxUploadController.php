<?php

namespace ArtistAdminBundle\Controller;

use ArtistAdminBundle\Entity\Uploading\File;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxUploadController extends Controller
{
    public function upload(Request $request)
    {
        /**
         * @var $em         EntityManager
         */


        $response = [];

        if ($request->files->get('file') && !empty($request->files->get('file')) && ($request->files->get('file') instanceof UploadedFile)) {
            $em = $this->getDoctrine()->getManager();
            $file = new File();
            $file->setSlug(File::FILE_USER_BLOG);
            $file->setObjFile($request->files->get('file'));
            $em->persist($file);


            try {

                $storageService = $this->get('vich_uploader.storage');

                $response = [
                    'location' => $this->get('vich_uploader.templating.helper.uploader_helper')->asset($file, 'objFile')
                ];

                $em->flush();

            } catch (\Doctrine\ORM\OptimisticLockException $exception) {

            }

        } else {

        }

        $jsonRes = new JsonResponse($response);
        $jsonRes->send();

        return $this->render('@view.art_admin/AjaxUpload/upload.html.twig', [

        ]);
    }

    public function userToken(Request $request)
    {
        return $this->render('@view.art_admin/AjaxUpload/userToken.html.twig', [
            'token' => $this->get('jwt')->getJWT(),
        ]);
    }

    public function getImages()
    {
        return $this->render('@view.art_admin/AjaxUpload/get_images.html.twig');
    }
}
