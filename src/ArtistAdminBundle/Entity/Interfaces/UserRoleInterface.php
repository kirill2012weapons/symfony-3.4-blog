<?php

namespace ArtistAdminBundle\Entity\Interfaces;


interface UserRoleInterface
{

    const ROLE_USER                 = 'ROLE_USER';
    const ROLE_SIMPLE_USER          = 'ROLE_SIMPLE_USER';
    const ROLE_MANAGER              = 'ROLE_MANAGER';
    const ROLE_ADMIN                = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN          = 'ROLE_SUPER_ADMIN';

}