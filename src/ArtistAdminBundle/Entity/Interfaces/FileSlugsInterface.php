<?php

namespace ArtistAdminBundle\Entity\Interfaces;


interface FileSlugsInterface
{

    const FILE_USER_BLOG                            = 'FILE_USER_BLOG';
    const FILE_USER_BLOG_THUMBNAIL                  = 'FILE_USER_BLOG_THUMBNAIL';
    const FILE_USER_LOGO                            = 'FILE_USER_LOGO';
    const FILE_GALLERY                              = 'FILE_GALLERY';

    const FILE_PROFILE_USER_LOGO                    = 'FILE_PROFILE_USER_LOGO';

}