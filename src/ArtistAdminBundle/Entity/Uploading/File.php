<?php

namespace ArtistAdminBundle\Entity\Uploading;


use ArtistAdminBundle\Entity\Interfaces\FileSlugsInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File as FileSymfony;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Naming\UniqidNamer;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class File implements FileSlugsInterface
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=100)
     * @var string
     */
    private $slug;

    /**
     * @Vich\UploadableField(mapping="uploads_main", fileNameProperty="file")
     * @var FileSymfony
     */
    private $objFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct() {}

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    public function setObjFile(FileSymfony $image = null)
    {
        $this->objFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getObjFile()
    {
        return $this->objFile;
    }

    public function setFile($file)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

}