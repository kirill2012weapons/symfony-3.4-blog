<?php

namespace ArtistAdminBundle\Entity\Options\Interfaces;


interface FacebookInterface
{

    const FACEBOOK_USER_URL          = 'facebook_user_url';

}