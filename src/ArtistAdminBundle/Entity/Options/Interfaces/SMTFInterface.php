<?php

namespace ArtistAdminBundle\Entity\Options\Interfaces;


interface SMTFInterface
{

    const SMTP_MAILER_HOST          = 'smtp_mailer_host';
    const SMTP_MAILER_USER          = 'smtp_mailer_user';
    const SMTP_MAILER_PASSWORD      = 'smtp_mailer_password';
    const SMTP_MAILER_PORT          = 'smtp_mailer_port';
    const SMTP_MAILER_EMAIL         = 'smtp_mailer_email';

}