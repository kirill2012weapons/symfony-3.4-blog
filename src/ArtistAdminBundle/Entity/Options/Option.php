<?php

namespace ArtistAdminBundle\Entity\Options;


use ArtistAdminBundle\Entity\Options\Interfaces\FacebookInterface;
use ArtistAdminBundle\Entity\Options\Interfaces\InstagramInterface;
use ArtistAdminBundle\Entity\Options\Interfaces\SMTFInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="art_options")
 * @ORM\Entity(repositoryClass="ArtistAdminBundle\Repository\OptionRepository")
 * @UniqueEntity(
 *     fields={"slug"}
 * )
 */
class Option implements FacebookInterface,
                        InstagramInterface,
                        SMTFInterface
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^[A-Za-z\_\-]{3,100}$/",
     *     message="Invalid slug."
     * )
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $value;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    public function __toString()
    {
        return $this->slug;
    }

}