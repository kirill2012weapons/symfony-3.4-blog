<?php

namespace ArtistAdminBundle\Entity\Gallery;

use ArtistAdminBundle\Entity\Interfaces\FileSlugsInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="art_blog_gallery")
 * @ORM\Entity(repositoryClass="ArtistAdminBundle\Repository\GalleryRepository")
 * @UniqueEntity(
 *     fields={"title"}
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class Gallery implements FileSlugsInterface
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250, unique=true)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^[\x{2E80}-\x{2FD5}\x{FF5F}-\x{FF9F}\x{3000}-\x{303F}\x{31F0}-\x{31FF}\x{FF01}-\x{FF5E}a-zA-Z0-9\p{Katakana}\p{Hiragana}\p{Han}\s\n\-\_\!\?\'\#\@\+]{3,200}/u",
     *     message="Enter latin."
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotNull()
     */
    private $isShow = true;

    /**
     * @ORM\Column(type="integer", name="`order`")
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^[0-9]{1,50}$/",
     *     message="Enter only number."
     * )
     */
    private $order = 1;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $helperInformation = 'Default Information';

    /**
     * @ORM\Column(type="integer", length=4)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^[0-9][0-9][0-9][0-9]$/",
     *     message="Enter only year."
     * )
     */
    private $year;

    /**
     * @ORM\Column(type="text")
     */
    private $informationContent;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modifiedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHelperInformation()
    {
        return $this->helperInformation;
    }

    /**
     * @param mixed $helperInformation
     */
    public function setHelperInformation($helperInformation)
    {
        $this->helperInformation = $helperInformation;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getisShow()
    {
        return $this->isShow;
    }

    /**
     * @param mixed $isShow
     */
    public function setIsShow($isShow)
    {
        $this->isShow = $isShow;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getInformationContent()
    {
        return $this->informationContent;
    }

    /**
     * @param mixed $informationContent
     */
    public function setInformationContent($informationContent)
    {
        $this->informationContent = $informationContent;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

}