<?php


namespace ArtistAdminBundle\Services\Factories\OptionFactory;



use ArtistAdminBundle\Entity\Options\Option;
use ArtistAdminBundle\Form\Options\OptionSMTP;
use ArtistAdminBundle\Repository\OptionRepository;
use Doctrine\ORM\EntityManager;

class OptionSMTPFactory
{

    public static function getFactory(EntityManager $entityManager)
	{
        /**
         * @var $em                 EntityManager
         * @var $optionRepository   OptionRepository
         */
        $em = $entityManager;
        $optionRepository = $em->getRepository( Option::class );

		$option = new OptionSMTP();

        $option->setUserHost(               $optionRepository->getOptionBySlug( Option::SMTP_MAILER_HOST ) );
        $option->setUserPass(               $optionRepository->getOptionBySlug( Option::SMTP_MAILER_PASSWORD ) );
        $option->setUserUsername(           $optionRepository->getOptionBySlug( Option::SMTP_MAILER_USER ) );
        $option->setUserPort(               $optionRepository->getOptionBySlug( Option::SMTP_MAILER_PORT ) );
        $option->setUserEmail(              $optionRepository->getOptionBySlug( Option::SMTP_MAILER_EMAIL ) );

		return $option;

	}

}
