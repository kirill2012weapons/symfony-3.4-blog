<?php


namespace ArtistAdminBundle\Services\Factories\OptionFactory;



use ArtistAdminBundle\Entity\Options\Option;
use ArtistAdminBundle\Form\Options\OptionInstagram;
use ArtistAdminBundle\Form\Options\OptionSMTP;
use ArtistAdminBundle\Repository\OptionRepository;
use Doctrine\ORM\EntityManager;

class OptionInstagramFactory
{

    public static function getFactory(EntityManager $entityManager)
	{
        /**
         * @var $em                 EntityManager
         * @var $optionRepository   OptionRepository
         */
        $em = $entityManager;
        $optionRepository = $em->getRepository( Option::class );

		$option = new OptionInstagram();

        $option->setUserUrl(               $optionRepository->getOptionBySlug( Option::INSTAGRAM_USER_URL ) );

		return $option;

	}

}
