<?php


namespace ArtistAdminBundle\Services\Factories;


use ArtistAdminBundle\Services\Node\Node;

class NodeManagerStaticFactory
{

	public static function createSiteInformationManager()
	{
		$nodeManager = new Node();

		$nodeManager->setTitle('Artist');
		$nodeManager->setDescription('Description default');

		return $nodeManager;

	}

}
