<?php

namespace ArtistAdminBundle\Services\Options;


use ArtistAdminBundle\Form\Options\OptionFacebook;
use ArtistAdminBundle\Form\Options\OptionInstagram;
use ArtistAdminBundle\Form\Options\OptionSMTP;
use Doctrine\ORM\EntityManager;

class Option
{

    /**
     * @var OptionSMTP
     */
    private $smtp;
    /**
     * @var OptionFacebook
     */
    private $facebook;
    /**
     * @var OptionInstagram
     */
    private $instagram;

    public function __construct($smtp, $facebook, $instagram)
    {
        $this->instagram = $instagram;
        $this->smtp = $smtp;
        $this->facebook = $facebook;
    }

    /**
     * @return OptionSMTP
     */
    public function getSmtp()
    {
        return $this->smtp;
    }

    /**
     * @return OptionFacebook
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @return OptionInstagram
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

}