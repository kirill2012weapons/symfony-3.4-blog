<?php

namespace ArtistAdminBundle\Services\Alerts\AlertEngine;


use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\Session;

abstract class AbstractAlert
{

    /**
     * @var FlashBag
     */
    private $flash;

    abstract public function success($success);
    abstract public function error($error);
    abstract public function info($info);
    abstract public function warning($warning);

    abstract public function getFlashName();

    public function __construct(Session $session)
    {
        $this->flash = $session->getFlashBag();
    }

    public function getMessage() {
        foreach ($this->flash->get( $this->getFlashName() ) as $key) echo $key;
    }

    protected function setMessage($message) {
        $this->flash->add( $this->getFlashName(), $message );
    }

}