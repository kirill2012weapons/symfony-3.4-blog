<?php

namespace ArtistAdminBundle\Services\Alerts;


use ArtistAdminBundle\Services\Alerts\AlertEngine\AbstractAlert;

class Alert extends AbstractAlert
{

    public function getFlashName()
    {
        return 'art_alerts';
    }

    public function error($error)
    {
        $alert = <<<alert
<script>
jQuery.wnoty({
  type: 'error',
  message: '{$error}',
  autohideDelay: 5000
});
</script>
alert;
        $this->setMessage($alert);
    }

    public function info($info)
    {
        $alert = <<<alert
<script>
jQuery.wnoty({
  type: 'info',
  message: '{$info}',
  autohideDelay: 5000
});
</script>
alert;
        $this->setMessage($alert);
    }

    public function success($success)
    {
        $alert = <<<alert
<script>
jQuery.wnoty({
  type: 'success',
  message: '{$success}',
  autohideDelay: 5000
});
</script>
alert;
        $this->setMessage($alert);
    }

    public function warning($warning)
    {
        $alert = <<<alert
<script>
jQuery.wnoty({
  type: 'warning',
  message: '{$warning}',
  autohideDelay: 5000
});
</script>
alert;
        $this->setMessage($alert);
    }

}