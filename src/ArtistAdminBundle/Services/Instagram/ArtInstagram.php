<?php

namespace ArtistAdminBundle\Services\Instagram;



use ArtistAdminBundle\Services\Instagram\Engine\AbstractArtInstagram;

class ArtInstagram extends AbstractArtInstagram
{

    public function getFeed($count)
    {
        if (!$this->isConnect()) return [
            'error' => true,
            'data'  => [],
        ];

        return [
            'error' => false,
            'data'  => [
                $this->getInstagram()->getUserMedia('self', $count)
            ]
        ];
    }

    public function getUserInfo()
    {
        if (!$this->isConnect()) return [
            'error' => true,
            'data'  => [],
        ];

        return [
            'error' => false,
            'data'  => [
                $this->getInstagram()->getUser()
            ]
        ];
    }

}