<?php

namespace ArtistAdminBundle\Services\Instagram\Engine;


use Doctrine\ORM\EntityManager;
use MetzWeb\Instagram\Instagram;

class AbstractArtInstagram
{

    /**
     * @var EntityManager
     */
    private $em;

    private $instaClientApiSecret;
    private $instaClientAccessToken;

    private $isConnect = false;

    private $instagram;

    public function __construct(EntityManager $entityManager, $instaClientApiSecret, $instaClientAccessToken)
    {
        $this->em                           = $entityManager;
        $this->instaClientAccessToken       = $instaClientAccessToken;
        $this->instaClientApiSecret         = $instaClientApiSecret;

        try {
            $this->instagram = new Instagram( $this->instaClientApiSecret );
            $this->instagram->setAccessToken( $this->instaClientAccessToken );
            $this->isConnect = true;
        } catch (\Exception $exception) {
            $this->isConnect = false;
        }

    }

    protected function getInstagram()
    {
        return $this->instagram;
    }

    public function isConnect()
    {
        return $this->isConnect;
    }

}