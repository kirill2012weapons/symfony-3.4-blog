<?php

namespace ArtistAdminBundle\Services\JWTAjaxUploader;


use ArtistAdminBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use \Firebase\JWT\JWT;

class AjaxUploader
{

    private $secretKey;
    private $envID;
    private $tokenStorage;

    /**
     * @var User
     */
    private $user;

    private $jwt;

    public function __construct($secretKey, $envID, TokenStorageInterface $tokenStorage)
    {
        $this->secretKey        = $secretKey;
        $this->envID            = $envID;

        $this->tokenStorage     = $tokenStorage;

        $this->user             = $tokenStorage->getToken()->getUser();
    }

    public function getJWT()
    {

        $payload = array(
            "iss" => $this->envID,
            "iat" => time(),
            'user' => array(
                'id' => $this->user->getId(),
                'email' => $this->user->getEmail(),
                'name' => $this->user->getUsername()
            )
        );

        return JWT::encode($payload, $this->secretKey);

    }
}