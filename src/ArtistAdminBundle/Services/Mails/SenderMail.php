<?php

namespace ArtistAdminBundle\Services\Mails;


use ArtistAdminBundle\Entity\Options\Option;
use ArtistAdminBundle\Form\Options\OptionSMTP;
use ArtistAdminBundle\Repository\OptionRepository;
use Doctrine\ORM\EntityManager;
use GetOptionKit\ValueType\EmailType;
use Symfony\Bundle\SwiftmailerBundle\DependencyInjection\SmtpTransportConfigurator;
use Symfony\Bundle\SwiftmailerBundle\DependencyInjection\SwiftmailerTransportFactory;

class SenderMail
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var \Swift_Mailer
     */
    private $mailler;
    private $smtpMailler;

    private $smtpOptions;

    private $swEvDispacher;

    public function __construct(EntityManager $em, $mailler, $smtpMailler, $smtpOptions, $swEvDispacher)
    {
        $this->em               = $em;
        $this->mailler          = $mailler;
        $this->smtpMailler      = $smtpMailler;
        $this->smtpOptions      = $smtpOptions;

        $this->swEvDispacher    = $swEvDispacher;


    }

    public function send(\Swift_Mime_SimpleMessage $message)
    {
        /**
         * @var $optRep             OptionRepository
         */
        $optRep = $this->em->getRepository(Option::class);

        /**
         * OPTIONS SMTP
         */
        $smtpClass = new OptionSMTP();
        $smtpClass->setUserHost(                $optRep->getOptionBySlug(Option::SMTP_MAILER_HOST) );
        $smtpClass->setUserUsername(            $optRep->getOptionBySlug(Option::SMTP_MAILER_USER) );
        $smtpClass->setUserPass(                $optRep->getOptionBySlug(Option::SMTP_MAILER_PASSWORD) );
        $smtpClass->setUserPort(                $optRep->getOptionBySlug(Option::SMTP_MAILER_PORT) );
        $smtpClass->setUserEmail(               $optRep->getOptionBySlug(Option::SMTP_MAILER_EMAIL) );


        $transport = SwiftmailerTransportFactory::createTransport([
            'transport'                     => 'smtp',
            'username'                      => $smtpClass->getUserUsername() ? $smtpClass->getUserUsername()->getValue() : '',
            'password'                      => $smtpClass->getUserPass() ? $smtpClass->getUserPass()->getValue() : '',
            'host'                          => $smtpClass->getUserHost() ? $smtpClass->getUserHost()->getValue() : '',
            'port'                          => $smtpClass->getUserPort() ? $smtpClass->getUserPort()->getValue() : '',

        ], null, $this->swEvDispacher);

        $mailer = new \Swift_Mailer($transport);

        $mailer->send($message);


    }

}