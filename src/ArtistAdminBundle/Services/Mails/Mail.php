<?php

namespace ArtistAdminBundle\Services\Mails;


use ArtistAdminBundle\Repository\MailRepository;
use Doctrine\ORM\EntityManager;

class Mail
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var MailRepository
     */
    private $mailRepository;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->mailRepository = $this->em->getRepository( \ArtistBundle\Entity\Mail::class );
    }

    public function getUnreadMessages()
    {
        return $this->mailRepository->getNoReadMailsSortedLast();
    }

}