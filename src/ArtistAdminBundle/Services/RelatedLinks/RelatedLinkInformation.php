<?php

namespace ArtistAdminBundle\Services\RelatedLinks;


use ArtistAdminBundle\Entity\Blog\Information;
use ArtistAdminBundle\Services\RelatedLinks\Engine\AbstractLinksManager;
use ArtistAdminBundle\Services\RelatedLinks\Engine\Interfaces\RelatedInterface;

class RelatedLinkInformation extends AbstractLinksManager
{

    protected function getNext($object)
    {

        /**
         * @var $object         Information
         * @var $result         Information
         */


        $result = $this
            ->entityManager
            ->createQueryBuilder()
            ->select( 'post' )
            ->andWhere('post.modifiedAt > :criteria')
            ->from(Information::class, 'post')
            ->orderBy('post.modifiedAt', 'ASC')
            ->setParameter('criteria', $object->getModifiedAt())
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;


        if (is_null($result)) return null;

        return $this->router->generate('artist_information_single', [
            'slug'      => $result->getTitle()
        ]);
    }

    protected function getPrev($object)
    {

        /**
         * @var $object         Information
         * @var $result         Information
         */


        $result = $this
            ->entityManager
            ->createQueryBuilder()
            ->select( 'post' )
            ->andWhere('post.modifiedAt < :criteria')
            ->from(Information::class, 'post')
            ->orderBy('post.modifiedAt', 'DESC')
            ->setParameter('criteria', $object->getModifiedAt())
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;


        if (is_null($result)) return null;

        return $this->router->generate('artist_information_single', [
            'slug'      => $result->getTitle()
        ]);
    }

}