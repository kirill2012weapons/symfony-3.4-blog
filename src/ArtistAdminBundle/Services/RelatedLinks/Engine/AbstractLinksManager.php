<?php

namespace ArtistAdminBundle\Services\RelatedLinks\Engine;


use ArtistAdminBundle\Entity\Blog\Information;
use ArtistAdminBundle\Services\RelatedLinks\Engine\Interfaces\RelatedInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Parameter;
use Symfony\Component\Routing\RouterInterface;

abstract class AbstractLinksManager
{

    private $prev = null;
    private $next = null;

    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var RouterInterface
     */
    protected $router;

    public function __construct($entityManager, $router)
    {
        $this->entityManager    = $entityManager;
        $this->router           = $router;
    }

    abstract protected function getNext($object);

    abstract protected function getPrev($object);

    public function getRelatedLinks($object)
    {

        return [
            'prev'  =>  $this->getPrev($object),
            'next'  =>  $this->getNext($object)
        ];

    }

}