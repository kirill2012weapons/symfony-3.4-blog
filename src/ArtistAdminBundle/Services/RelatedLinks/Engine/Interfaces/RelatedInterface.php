<?php

namespace ArtistAdminBundle\Services\RelatedLinks\Engine\Interfaces;


interface RelatedInterface
{

    public function getLink();

}