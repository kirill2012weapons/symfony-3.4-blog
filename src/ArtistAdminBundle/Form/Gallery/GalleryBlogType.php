<?php

namespace ArtistAdminBundle\Form\Gallery;


use ArtistAdminBundle\Entity\Blog\Information;
use ArtistAdminBundle\Entity\Gallery\Gallery;
use ArtistAdminBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class GalleryBlogType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isShow', CheckboxType::class, [
                'label'     => 'Is Show This post',
            ])
            ->add('title', TextType::class, [
                'label'     => 'Title',
            ])
            ->add('year', TextType::class, [
                'label'     => 'Year',
            ])
            ->add('informationContent', TextType::class, [
                'label'     => 'Information Content',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gallery::class,
        ]);
    }

    public function getName()
    {
        return 'gallery_blog_form';
    }

}