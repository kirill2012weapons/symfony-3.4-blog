<?php

namespace ArtistAdminBundle\Form\Security;


use ArtistAdminBundle\Entity\User;
use ArtistAdminBundle\Validator\Constraints\ContainsIsPasswordEqual;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class UserPasswordType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', PasswordType::class, [
                'label'         => 'Password',
                'mapped'        => false,
                'constraints'   => [
                    new ContainsIsPasswordEqual(),
                    new Assert\NotBlank()
                ],
            ])
            ->add('newPassword', RepeatedType::class, [
                'constraints'   => [
                    new Assert\NotBlank()
                ],
                'label'     => 'Password',
                'mapped'    => false,
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'required' => true,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    public function getName()
    {
        return 'user_password_form';
    }

}