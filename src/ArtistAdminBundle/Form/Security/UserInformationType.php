<?php

namespace ArtistAdminBundle\Form\Security;


use ArtistAdminBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class UserInformationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('logo', FileType::class, [
                'label'         => 'Logo Image',
                'mapped'        => false,
                'constraints'   => [
                    new Assert\Image([
                        'mimeTypes'     => ['image/jpeg', 'image/png']
                    ]),
                ],
            ])
            ->add('username', TextType::class, [
                'label'     => 'User Name',
            ])
            ->add('name', TextType::class, [
                'label'     => 'Name',
            ])
            ->add('surname', TextType::class, [
                'label'     => 'Surname',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    public function getName()
    {
        return 'user_information_form';
    }

}