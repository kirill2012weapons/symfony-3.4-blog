<?php

namespace ArtistAdminBundle\Form\Options;


use ArtistAdminBundle\Entity\Options\Option;
use ArtistAdminBundle\Form\Options\Types\OptionMailerHostType;
use ArtistAdminBundle\Form\Options\Types\OptionPasswordType;
use ArtistAdminBundle\Form\Options\Types\OptionUrlInstagramType;
use ArtistAdminBundle\Form\Options\Types\OptionUsernameType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class OptionInstagramType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userUrl', OptionUrlInstagramType::class, [
                'error_bubbling'    => false,
            ])
        ;
    }

    public function getName()
    {
        return 'options_instagram_type';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OptionInstagram::class,
        ]);
    }

}