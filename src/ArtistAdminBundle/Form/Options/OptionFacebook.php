<?php

namespace ArtistAdminBundle\Form\Options;



class OptionFacebook
{

    private $userUrl;

    /**
     * @return mixed
     */
    public function getUserUrl()
    {
        return $this->userUrl;
    }

    /**
     * @param mixed $userUrl
     */
    public function setUserUrl($userUrl)
    {
        $this->userUrl = $userUrl;
    }

}