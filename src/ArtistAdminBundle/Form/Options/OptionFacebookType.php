<?php

namespace ArtistAdminBundle\Form\Options;


use ArtistAdminBundle\Form\Options\Types\OptionUrlFacebookType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OptionFacebookType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userUrl', OptionUrlFacebookType::class, [
                'error_bubbling'    => false,
            ])
        ;
    }

    public function getName()
    {
        return 'options_facebook_type';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OptionFacebook::class,
        ]);
    }

}