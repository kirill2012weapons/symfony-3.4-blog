<?php

namespace ArtistAdminBundle\Form\Options;


use ArtistAdminBundle\Entity\Options\Option;
use ArtistAdminBundle\Form\Options\Types\OptionMailerEmailType;
use ArtistAdminBundle\Form\Options\Types\OptionMailerHostType;
use ArtistAdminBundle\Form\Options\Types\OptionMailerPortType;
use ArtistAdminBundle\Form\Options\Types\OptionPasswordType;
use ArtistAdminBundle\Form\Options\Types\OptionUsernameType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class OptionSMTPType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userEmail',          OptionMailerEmailType::class, [
                'error_bubbling'    => false,
            ])
            ->add('userHost',          OptionMailerHostType::class, [
                'error_bubbling'    => false,
            ])
            ->add('userUsername',      OptionUsernameType::class, [
                'error_bubbling'    => false,
            ])
            ->add('userPass',           OptionPasswordType::class, [
                'error_bubbling'    => false,
            ])
            ->add('userPort',           OptionMailerPortType::class, [
                'error_bubbling'    => false,
            ])
        ;
    }

    public function getName()
    {
        return 'options_smtp_host_type';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OptionSMTP::class,
        ]);
    }

}