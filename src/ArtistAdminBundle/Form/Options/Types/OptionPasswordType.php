<?php

namespace ArtistAdminBundle\Form\Options\Types;


use ArtistAdminBundle\Entity\Options\Option;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class OptionPasswordType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', TextType::class, [
                'label'         => 'Slug',
                'data'          => Option::SMTP_MAILER_PASSWORD,
                'empty_data'    => Option::SMTP_MAILER_PASSWORD,
                'constraints'   => [
                    new Assert\NotBlank(),
                    new Assert\IdenticalTo([
                        'value' => Option::SMTP_MAILER_PASSWORD,
                    ])
                ]
            ])
            ->add('value', TextType::class, [
                'label'     => 'Value',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Option::class,
        ]);
    }

    public function getName()
    {
        return 'options_password_type';
    }

}