<?php

namespace ArtistAdminBundle\Form\Options\Types;


use ArtistAdminBundle\Entity\Options\Option;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class OptionUrlFacebookType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', TextType::class, [
                'label'         => 'Slug',
                'data'          => Option::FACEBOOK_USER_URL,
                'empty_data'    => Option::FACEBOOK_USER_URL,
                'constraints'   => [
                    new Assert\NotBlank(),
                    new Assert\IdenticalTo([
                        'value' => Option::FACEBOOK_USER_URL,
                    ])
                ]
            ])
            ->add('value', TextType::class, [
                'label'     => 'Value',
                'constraints'    => [
                    new Assert\Url(),
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Option::class,
        ]);
    }

    public function getName()
    {
        return 'options_facebook_url_type';
    }

}