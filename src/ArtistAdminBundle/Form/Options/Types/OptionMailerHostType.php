<?php

namespace ArtistAdminBundle\Form\Options\Types;


use ArtistAdminBundle\Entity\Options\Option;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class OptionMailerHostType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('slug', TextType::class, [
                'label'         => 'Slug',
                'data'          => Option::SMTP_MAILER_HOST,
                'empty_data'    => Option::SMTP_MAILER_HOST,
                'constraints'   => [
                    new Assert\NotBlank(),
                    new Assert\IdenticalTo([
                        'value' => Option::SMTP_MAILER_HOST,
                    ])
                ]
            ])
            ->add('value', TextType::class, [
                'label'     => 'Value',
                'constraints'    => [
                    new Assert\Length([
                        'min'   => 3,
                        'max'   => 250,
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Option::class,
        ]);
    }

    public function getName()
    {
        return 'options_smtp_host_type';
    }

}