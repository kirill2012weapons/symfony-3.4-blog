<?php

namespace ArtistAdminBundle\Form\Options\Types;


use ArtistAdminBundle\Entity\Options\Option;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class OptionMailerPortType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('slug', TextType::class, [
                'label'         => 'Slug',
                'data'          => Option::SMTP_MAILER_PORT,
                'empty_data'    => Option::SMTP_MAILER_PORT,
                'constraints'   => [
                    new Assert\NotBlank(),
                    new Assert\IdenticalTo([
                        'value' => Option::SMTP_MAILER_PORT,
                    ])
                ]
            ])
            ->add('value', TextType::class, [
                'label'     => 'Value',
                'constraints'    => [
                    new Assert\Regex([
                        'pattern'   => '/^[0-9]+$/',
                        'message'   => 'Must bee number',
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Option::class,
        ]);
    }

    public function getName()
    {
        return 'options_smtp_port_type';
    }

}