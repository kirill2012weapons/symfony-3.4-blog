<?php

namespace ArtistAdminBundle\Form\Options;



use ArtistAdminBundle\Entity\Options\Option;

class OptionSMTP
{

    private $userHost;
    private $userPass;
    private $userUsername;
    private $userPort;
    private $userEmail;

    /**
     * @return Option
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @param mixed $userEmail
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;
    }

    /**
     * @return Option
     */
    public function getUserPort()
    {
        return $this->userPort;
    }

    /**
     * @param mixed $userPort
     */
    public function setUserPort($userPort)
    {
        $this->userPort = $userPort;
    }

    /**
     * @return Option
     */
    public function getUserHost()
    {
        return $this->userHost;
    }

    /**
     * @param mixed $userHost
     */
    public function setUserHost($userHost)
    {
        $this->userHost = $userHost;
    }

    /**
     * @return Option
     */
    public function getUserPass()
    {
        return $this->userPass;
    }

    /**
     * @param mixed $userPass
     */
    public function setUserPass($userPass)
    {
        $this->userPass = $userPass;
    }

    /**
     * @return Option
     */
    public function getUserUsername()
    {
        return $this->userUsername;
    }

    /**
     * @param mixed $userUsername
     */
    public function setUserUsername($userUsername)
    {
        $this->userUsername = $userUsername;
    }

}