<?php

namespace ArtistAdminBundle\Form\Information;


use ArtistAdminBundle\Entity\Blog\Information;
use ArtistAdminBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints as Assert;

class BlogInformationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label'     => 'Title',
            ])
            ->add('informationContent', TextareaType::class, [
                'label'     => 'Information Content',
                'attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'bbcode'
                )
            ])
            ->add('thumbnail', FileType::class, [
                'label'         => 'Thumbnail Image',
                'mapped'        => false,
                'constraints'   => [
                    new Assert\Image([
                        'mimeTypes'     => ['image/jpeg', 'image/png']
                    ]),
                ],
            ])
        ;

//        $builder->add('field', CKEditorType::class, array(
//            'autoload' => false,
//            'async'    => true,
//        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Information::class,
        ]);
    }

    public function getName()
    {
        return 'information_blog_form';
    }

}