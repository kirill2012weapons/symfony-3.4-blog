<?php

namespace ArtistAdminBundle\Repository;


use Doctrine\ORM\EntityRepository;

class MailRepository extends EntityRepository
{

    public function getAllMailsSortedLast()
    {

        return $this
            ->createQueryBuilder('mail')
            ->select('mail')
            ->orderBy('mail.createdAt', 'DESC')
            ->getQuery()
            ->getArrayResult();

    }

    public function getNoReadMailsSortedLast()
    {

        return $this
            ->createQueryBuilder('mail')
            ->select('mail')
            ->where('mail.isShow = 0')
            ->orderBy('mail.createdAt', 'DESC')
            ->getQuery()
            ->getArrayResult();

    }

}