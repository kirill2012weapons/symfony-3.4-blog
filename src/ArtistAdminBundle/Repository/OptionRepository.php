<?php

namespace ArtistAdminBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

class OptionRepository extends EntityRepository
{
    public function getOptionBySlug($slug)
    {
        return $this
            ->createQueryBuilder('option')
            ->select('option')
            ->where('option.slug = :slug')
            ->setMaxResults(1)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}