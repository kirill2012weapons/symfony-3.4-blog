<?php

namespace ArtistAdminBundle\Repository;


use ArtistAdminBundle\Entity\Gallery\Gallery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class GalleryRepository extends EntityRepository
{

    public function getListOfPostGallaryAsArray()
    {
        return $this
            ->createQueryBuilder('gallery')
            ->select('gallery.year')
            ->orderBy('gallery.year', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getListOfPostGallaryAsArrayGroupByYear()
    {
        return $this
            ->createQueryBuilder('gallery')
            ->select('gallery.year')
            ->orderBy('gallery.year', 'ASC')
            ->groupBy('gallery.year')
            ->getQuery()
            ->getArrayResult();
    }

    public function getPostByYearAsArray($year)
    {

        if (empty($year)) {
            return $this
                ->createQueryBuilder('gallery')
                ->select('gallery')
                ->andWhere(
                    'gallery.year =' . $this->getEntityManager()
                                            ->createQueryBuilder()
                                            ->select('gallery.year')
                                            ->from(Gallery::class, 'gallery')
                                            ->setMaxResults(1)
                                            ->orderBy('gallery.year', 'DESC')
                                            ->getQuery()
                                            ->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR)
                )
                ->andWhere('gallery.isShow = 1')
                ->addOrderBy('gallery.order',       'ASC')
                ->addOrderBy('gallery.createdAt',   'DESC')
                ->getQuery()
                ->getArrayResult()
                ;

        } else {
            return $this
                ->createQueryBuilder('gallery')
                ->select('gallery')
                ->orderBy('gallery.year', 'DESC')
                ->where('gallery.year = :year')
                ->andWhere('gallery.isShow = 1')
                ->setParameter('year', $year)
                ->getQuery()
                ->getArrayResult()
                ;

        }

    }

    public function getAllPostsGalleryAsArray()
    {
        return $this
            ->createQueryBuilder('gallery')
            ->select('gallery')
            ->orderBy('gallery.createdAt', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

}