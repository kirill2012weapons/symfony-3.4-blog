<?php

namespace ArtistAdminBundle\Repository;


use ArtistAdminBundle\Entity\Blog\Profile;
use ArtistAdminBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class ProfileRepository extends EntityRepository
{

    public function getFirstOrNewProfileResult()
    {
        $profile = $this->createQueryBuilder('profile')
                    ->select('profile')
                    ->setMaxResults(1)
                    ->orderBy('profile.createdAt', 'DESC')
                    ->getQuery()
                    ->getOneOrNullResult(Query::HYDRATE_OBJECT)
        ;

        if (is_null($profile)) return new Profile();
        else return $profile;
    }

}