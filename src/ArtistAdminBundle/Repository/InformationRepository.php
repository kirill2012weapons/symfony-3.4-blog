<?php

namespace ArtistAdminBundle\Repository;


use ArtistAdminBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class InformationRepository extends EntityRepository
{

    public function getAllInformationAllAsArray($sort = 'DESC')
    {
        return $this
            ->createQueryBuilder('information')
            ->select('information')
            ->innerJoin('information.user', 'user')
            ->addSelect('user')
            ->orderBy('information.modifiedAt', $sort)
            ->getQuery()
            ->getArrayResult()
            ;
    }

    public function getAllInformationByCountAsArray($count = 5, $sort = 'DESC')
    {
        return $this
            ->createQueryBuilder('information')
            ->select('information')
            ->innerJoin('information.user', 'user')
            ->addSelect('user')
            ->orderBy('information.modifiedAt', $sort)
            ->setMaxResults($count)
            ->getQuery()
            ->getArrayResult()
            ;
    }

    public function findByTitle($title)
    {
        return $this
            ->createQueryBuilder('information')
            ->select('information')
            ->where('information.title = :title')
            ->innerJoin('information.user', 'user')
            ->addSelect('user')
            ->setParameters([
                'title' => $title
            ])
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

}