<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190815111812 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, file VARCHAR(255) NOT NULL, slug VARCHAR(100) NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE art_users ADD logo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE art_users ADD CONSTRAINT FK_743A0B4AF98F144A FOREIGN KEY (logo_id) REFERENCES file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_743A0B4AF98F144A ON art_users (logo_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE art_users DROP FOREIGN KEY FK_743A0B4AF98F144A');
        $this->addSql('DROP TABLE file');
        $this->addSql('DROP INDEX UNIQ_743A0B4AF98F144A ON art_users');
        $this->addSql('ALTER TABLE art_users DROP logo_id');
    }
}
