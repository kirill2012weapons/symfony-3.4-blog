<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190819080205 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE art_blog_information (id INT AUTO_INCREMENT NOT NULL, thumbnail_id INT DEFAULT NULL, user_id INT DEFAULT NULL, title VARCHAR(250) NOT NULL, information_content LONGTEXT NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_1B6BB42B2B36786B (title), UNIQUE INDEX UNIQ_1B6BB42BFDFF2E92 (thumbnail_id), INDEX IDX_1B6BB42BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE art_blog_information ADD CONSTRAINT FK_1B6BB42BFDFF2E92 FOREIGN KEY (thumbnail_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE art_blog_information ADD CONSTRAINT FK_1B6BB42BA76ED395 FOREIGN KEY (user_id) REFERENCES art_users (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE art_blog_information');
    }
}
