-- MySQL dump 10.13  Distrib 5.6.43, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: sy_145
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.33-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `art_blog_gallery`
--

DROP TABLE IF EXISTS `art_blog_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_blog_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL,
  `order` int(11) NOT NULL,
  `helper_information` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `information_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D879E1B22B36786B` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_blog_gallery`
--

LOCK TABLES `art_blog_gallery` WRITE;
/*!40000 ALTER TABLE `art_blog_gallery` DISABLE KEYS */;
INSERT INTO `art_blog_gallery` (`id`, `title`, `is_show`, `order`, `helper_information`, `year`, `information_content`, `created_at`, `modified_at`) VALUES (1,'Gallery 1',0,1,'Default Information',2017,'<p style=\"text-align:center\"><img alt=\"\" height=\"624\" src=\"/uploads/5d665e1052893.jpg\" width=\"624\" /></p>\r\n\r\n<p style=\"text-align:center\">Text 1&nbsp;</p>','2019-08-28 12:46:26','2019-08-28 14:07:18'),(2,'Gallery 2',1,2,'Default Information',2017,'<p style=\"text-align:center\"><img alt=\"\" height=\"983\" src=\"/uploads/5d665eaaf017f.jpg\" width=\"983\" /></p>\r\n\r\n<p>Text 2&nbsp;</p>','2019-08-28 13:00:09','2019-08-28 14:07:43'),(3,'Gallery 3',1,1,'Default Information',2016,'<p style=\"text-align:center\"><img alt=\"\" height=\"983\" src=\"/uploads/5d665ed7b0e8d.jpg\" width=\"982\" /></p>\r\n\r\n<p style=\"text-align:center\">asdasdasd</p>','2019-08-28 13:01:10','2019-08-28 14:07:54'),(4,'Gallery 4',1,1,'Default Information',2020,'<p style=\"text-align:center\"><img alt=\"\" height=\"982\" src=\"/uploads/5d66675bbda0a.jpg\" width=\"983\" /></p>\r\n\r\n<p>&nbsp;</p>','2019-08-28 13:37:10','2019-08-30 09:50:56');
/*!40000 ALTER TABLE `art_blog_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `art_blog_information`
--

DROP TABLE IF EXISTS `art_blog_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_blog_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `information_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1B6BB42B2B36786B` (`title`),
  UNIQUE KEY `UNIQ_1B6BB42BFDFF2E92` (`thumbnail_id`),
  KEY `IDX_1B6BB42BA76ED395` (`user_id`),
  CONSTRAINT `FK_1B6BB42BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `art_users` (`id`),
  CONSTRAINT `FK_1B6BB42BFDFF2E92` FOREIGN KEY (`thumbnail_id`) REFERENCES `file` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_blog_information`
--

LOCK TABLES `art_blog_information` WRITE;
/*!40000 ALTER TABLE `art_blog_information` DISABLE KEYS */;
INSERT INTO `art_blog_information` (`id`, `thumbnail_id`, `user_id`, `title`, `information_content`, `created_at`, `modified_at`) VALUES (2,21,1,'Sasha Hui','<figure class=\"easyimage easyimage-side\"><img alt=\"\" sizes=\"100vw\" src=\"/uploads/5d5be7dfcee48.jpg\" srcset=\"\" width=\"1081\" />\r\n<figcaption></figcaption>\r\n</figure>\r\n\r\n<h1>Привет меня зовут кирил</h1>\r\n\r\n<p><strong>А вот и я старик)</strong></p>\r\n\r\n<figure class=\"easyimage easyimage-side\"><img alt=\"\" sizes=\"100vw\" src=\"/uploads/5d5bea7301c68.png\" srcset=\"\" width=\"254\" />\r\n<figcaption></figcaption>\r\n</figure>\r\n\r\n<p>ssssssssssssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss ssssssssssssssssss sssssssssssssssssssssssssssssss ssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssss ssssssssssssssssssssssssss sssssssssss sssssssssssssssssssssssssssssssss ssssssssssssssssssss sssssssssssssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssssssssssssssssss sssssssssssss ssssssssssssssssssssssssssssssssssssssss sssssssssssssssssssssssssssss ssssssssssssssssssssssss ssssssssssssssssssssssssssssssssssssssssssssssss sssssssss sssssssssssssssssssssssssssssssssssssssssssssssssssssss ssssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssssssssssssssssssssss ssssssssssssssssssssss ssssssssssssssssssssssss sssssssssssssssssssss ssssssssssssssssssssssssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssssssssssssssssssssssssss sssssssssssssssssssssssssssssssssssssssssssssssssssssss ssssssssssssssssssssssssss</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>asdasd</td>\r\n			<td>asdasd</td>\r\n		</tr>\r\n		<tr>\r\n			<td>asdasd</td>\r\n			<td>asdasd</td>\r\n		</tr>\r\n		<tr>\r\n			<td>asd</td>\r\n			<td>asdasdasda</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>','2019-08-19 16:32:30','2019-08-20 14:45:57'),(4,40,1,'asdasdasdasd','<p>new</p>','2019-08-20 14:55:39','2019-08-20 14:55:39'),(5,41,1,'test title two','<p>tes title</p>\r\n\r\n<figure class=\"easyimage easyimage-full\"><img alt=\"\" src=\"blob:http://sy-145.local.com/7b1c2605-9bd7-4a81-994d-b336e66ac4bb\" width=\"1920\" />\r\n<figcaption></figcaption>\r\n</figure>\r\n\r\n<p>&nbsp;</p>','2019-08-20 15:00:13','2019-08-20 15:00:13'),(6,42,1,'Test title 4','<p>asdasdasdasdasd</p>','2019-08-20 15:00:34','2019-08-20 15:00:34'),(7,43,1,'invalid thiomes','<p>фывфывфывфыв</p>','2019-08-20 15:03:29','2019-08-20 15:03:29'),(8,44,1,'Invalid 2','<p>asdasdasd asd asd asd</p>','2019-08-20 15:03:45','2019-08-20 15:03:45'),(9,NULL,1,'some else','<p>asdasdas as sa as&nbsp;</p>\r\n\r\n<figure class=\"easyimage easyimage-full\"><img alt=\"\" sizes=\"100vw\" src=\"/uploads/5d5e40c6d7b9e.jpg\" srcset=\"\" width=\"1081\" />\r\n<figcaption></figcaption>\r\n</figure>\r\n\r\n<p>&nbsp;</p>','2019-08-20 15:04:00','2019-08-22 09:14:20'),(10,54,1,'title 1','<p><img alt=\"\" height=\"300\" src=\"/uploads/5d5e940d3531b.jpg\" style=\"float:right\" width=\"400\" /></p>\r\n\r\n<h1><strong>Это моя команда</strong></h1>\r\n\r\n<p>фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфыв&nbsp;фывфывфывфывфывфыв&nbsp;фывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфывфыв</p>\r\n\r\n<p><img alt=\"\" src=\"/uploads/5d5e942554cff.jpg\" width=\"1081\" /></p>\r\n\r\n<p><img alt=\"\" height=\"451\" src=\"/uploads/5d65302e0a7a1.jpg\" width=\"451\" /></p>','2019-08-22 15:10:36','2019-08-27 15:30:43'),(11,NULL,1,'asdasda 1','<p>asdasdasd</p>','2019-08-28 12:31:20','2019-08-28 12:31:20'),(12,NULL,1,'asdasdaa aa','<p>asdasdasdasd</p>','2019-08-28 12:32:08','2019-08-28 12:32:08'),(13,NULL,1,'aas11','<p>asdasd</p>','2019-08-28 12:33:34','2019-08-28 12:33:34');
/*!40000 ALTER TABLE `art_blog_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `art_blog_profile`
--

DROP TABLE IF EXISTS `art_blog_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_blog_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail_id` int(11) DEFAULT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `information_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1E0533872B36786B` (`title`),
  UNIQUE KEY `UNIQ_1E053387FDFF2E92` (`thumbnail_id`),
  CONSTRAINT `FK_1E053387FDFF2E92` FOREIGN KEY (`thumbnail_id`) REFERENCES `file` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_blog_profile`
--

LOCK TABLES `art_blog_profile` WRITE;
/*!40000 ALTER TABLE `art_blog_profile` DISABLE KEYS */;
INSERT INTO `art_blog_profile` (`id`, `thumbnail_id`, `title`, `description`, `information_content`, `created_at`, `modified_at`) VALUES (2,66,'米蒸 千穂 Chiho Yonemushi','1984 埼玉県生まれ\r\n2002 私立自由の森学園高校卒業\r\n2006 明星大学日本文化学部造形芸術学科 日本画専攻卒業apper\r\n2008 早見芸術学園造形研究所日本画塾卒業','<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>1984</td>\r\n			<td>埼玉県生まれ</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2002</td>\r\n			<td>私立自由の森学園高校卒業</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2006</td>\r\n			<td>明星大学日本文化学部造形芸術学科 日本画専攻卒業</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2008</td>\r\n			<td>早見芸術学園造形研究所日本画塾卒業</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h3><strong>[展示]</strong></h3>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>2005</td>\r\n			<td>青梅アートフェスティバル「膠と油展」（青梅）</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2006</td>\r\n			<td>明星大学卒業制作展（大学内）<br />\r\n			あしたとえ展（PROMO-ARTE 青山）<br />\r\n			9th Contemporary Young Painters Exhibition From Japan（バンググラディシュ）<br />\r\n			アジアの岐路展（中和ギャラリー 銀座）</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2007</td>\r\n			<td>早見芸術学園日本画塾12期生グループ展（鎌倉）<br />\r\n			花まつり（ジェイトリップギャラリー 代官山）<br />\r\n			MAY QUEENS（山脇ギャラリー）<br />\r\n			Contemporary Artists 4（バングラディシュ）</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2008</td>\r\n			<td>早見芸術学園日本画塾12期生卒業制作展（LIVEandMORIS銀座）<br />\r\n			漣の会 （藤沢さいか屋）以後2009、2010、2012参加<br />\r\n			クリスマス会 （川崎さいか屋・銀座松坂屋）</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2009</td>\r\n			<td>青空でアート（泰明小学校）<br />\r\n			クリスマス会（銀座松坂屋）</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2011</td>\r\n			<td>If We Hold on Together 絆&minus;東日本大震災チャリティーオークション（上海）</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2014</td>\r\n			<td>新年万福！お年賀展 (FEI ARTMUSEUM YOKOHAMA)&nbsp;<br />\r\n			-女子会-現代日本画七つ星展(シルクランド画廊)<br />\r\n			OKIMOCHI展 (FEI ARTGALLERY YOKOHAMA)<br />\r\n			MSDZ (ギャラリーオノマトペ 阿佐ヶ谷)<br />\r\n			第18回 創画会東京研究所 夏の会(ギャラリー青羅 銀座)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2015</td>\r\n			<td>新年万福！お年賀展 (FEI ARTMUSEUM YOKOHAMA)<br />\r\n			MSDZ2 (ギャラリーオノマトペ 阿佐ヶ谷)<br />\r\n			ジタン館 グループ展 (鎌倉)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2016</td>\r\n			<td>新年万福！お年賀展 (FEI ARTMUSEUM YOKOHAMA)<br />\r\n			クロスビューアート展</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2017</td>\r\n			<td>SPLASH展（シバヤマアートギャラリー）<br />\r\n			SPLASH展（東京大丸美術画廊）</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2019</td>\r\n			<td>若手作家3人展（横浜そごう美術画廊）<br />\r\n			SPLASH2展（シバヤマアートギャラリー）</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h3><strong>[個展]</strong></h3>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>2006</td>\r\n			<td>埼玉県生まれ</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2007</td>\r\n			<td>\r\n			<p>私立自由の森学園高校卒業</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2010.2011.<br />\r\n			2013</td>\r\n			<td>San-Ai-Gallery(人形町)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2016</td>\r\n			<td>ギャラリーなつか（京橋）</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2018</td>\r\n			<td>コートギャラリー（国立）</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2019</td>\r\n			<td>ギャラリー８８（国立）</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h3><strong>[公募展]</strong></h3>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>2005</td>\r\n			<td>第三回トリエンナーレ豊橋 星野眞吾賞展入選</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2012.2013.<br />\r\n			2015-2019</td>\r\n			<td>東京春季創画展入選</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2012-2018</td>\r\n			<td>創画展入選</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2017</td>\r\n			<td>第七回トリエンナーレ豊橋 星野眞吾賞展入選</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2019</td>\r\n			<td>FACE展 損保ジャパン日本興亜美術賞展入選</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style=\"text-align:right\">現在創画会会友</p>\r\n\r\n<p style=\"text-align:right\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">My Page</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" height=\"1080\" src=\"/uploads/5d6cc1fa8d7a6.jpg\" width=\"1920\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>','2019-08-28 15:06:52','2019-09-02 09:17:45');
/*!40000 ALTER TABLE `art_blog_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `art_mail_messages`
--

DROP TABLE IF EXISTS `art_mail_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_mail_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_mail_messages`
--

LOCK TABLES `art_mail_messages` WRITE;
/*!40000 ALTER TABLE `art_mail_messages` DISABLE KEYS */;
INSERT INTO `art_mail_messages` (`id`, `name`, `phone`, `email`, `text`, `is_show`, `created_at`, `modified_at`) VALUES (1,'test','123123123','asdasd@asd.asd','asdasda\r\nsd\r\nasdasd',1,'2019-08-29 10:46:13','2019-08-29 12:29:25'),(2,'asdasdasd','123123123','asdasd@asd.asd','asdasdasdsad\r\nasd\r\nas\r\ndas\r\ndasdasd',1,'2019-08-30 09:44:19','2019-08-30 09:44:43');
/*!40000 ALTER TABLE `art_mail_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `art_options`
--

DROP TABLE IF EXISTS `art_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6471FBFB989D9B62` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_options`
--

LOCK TABLES `art_options` WRITE;
/*!40000 ALTER TABLE `art_options` DISABLE KEYS */;
INSERT INTO `art_options` (`id`, `slug`, `value`) VALUES (1,'smtp_mailer_host','mail.adm.tools'),(4,'smtp_mailer_user','test@izumi-it-company.com'),(5,'smtp_mailer_password','80661053298k!'),(7,'instagram_user_url','https://instagram.com'),(8,'facebook_user_url','https://facebook.com'),(9,'smtp_mailer_port','2525'),(10,'smtp_mailer_email','kirill2012weapons@gmail.com');
/*!40000 ALTER TABLE `art_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `art_users`
--

DROP TABLE IF EXISTS `art_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `logo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_743A0B4AF85E0677` (`username`),
  UNIQUE KEY `UNIQ_743A0B4AE7927C74` (`email`),
  UNIQUE KEY `UNIQ_743A0B4AF98F144A` (`logo_id`),
  CONSTRAINT `FK_743A0B4AF98F144A` FOREIGN KEY (`logo_id`) REFERENCES `file` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `art_users`
--

LOCK TABLES `art_users` WRITE;
/*!40000 ALTER TABLE `art_users` DISABLE KEYS */;
INSERT INTO `art_users` (`id`, `username`, `password`, `email`, `name`, `surname`, `is_active`, `roles`, `created_at`, `modified_at`, `logo_id`) VALUES (1,'Chiho1','$2y$13$LegybxIhZ7crDC022ZWqveaBrPYRsrfTNS6JLzSx7CEkOUIJju9lC','kirill2012weapons@gmail.com','Chihooo','Yonemushi',1,'a:2:{i:0;s:10:\"ROLE_ADMIN\";i:1;s:10:\"ROLE_ADMIN\";}','2019-08-15 09:35:01','2019-08-30 09:45:49',67);
/*!40000 ALTER TABLE `art_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
INSERT INTO `file` (`id`, `file`, `slug`, `updated_at`) VALUES (4,'5d554b0135880.png','FILE_USER_LOGO','2019-08-15 14:07:29'),(5,'5d554b71e5038.png','FILE_USER_LOGO','2019-08-15 14:09:21'),(6,'5d554c032d091.png','FILE_USER_LOGO','2019-08-15 14:11:47'),(7,'5d554c1287895.png','FILE_USER_LOGO','2019-08-15 14:12:02'),(8,'5d554c1f623bb.png','FILE_USER_LOGO','2019-08-15 14:12:15'),(9,'5d554c3133b4e.png','FILE_USER_LOGO','2019-08-15 14:12:33'),(10,'5d554f3bb8ed6.jpg','FILE_USER_LOGO','2019-08-15 14:25:31'),(11,'5d554f6136af7.png','FILE_USER_LOGO','2019-08-15 14:26:09'),(12,'5d554f7e14832.jpg','FILE_USER_LOGO','2019-08-15 14:26:38'),(13,'5d554fb47b64c.jpg','FILE_USER_LOGO','2019-08-15 14:27:32'),(14,'5d555071790b4.png','FILE_USER_LOGO','2019-08-15 14:30:41'),(15,'5d5a4cf4c8070.jpg','FILE_USER_LOGO','2019-08-19 09:17:08'),(16,'5d5a4e234985b.png','FILE_USER_LOGO','2019-08-19 09:22:11'),(17,'5d5aa8e5b5654.jpg','FILE_USER_BLOG_THUMBNAIL','2019-08-19 15:49:25'),(18,'5d5aa9105b144.jpg','FILE_USER_LOGO','2019-08-19 15:50:08'),(19,'5d5ab2fe2fbaa.jpg','FILE_USER_BLOG_THUMBNAIL','2019-08-19 16:32:30'),(20,'5d5be03bba35d.png','FILE_USER_BLOG','2019-08-20 13:57:47'),(21,'5d5be171d3b95.jpg','FILE_USER_BLOG_THUMBNAIL','2019-08-20 14:02:57'),(22,'5d5be210cca9c.jpg','FILE_USER_BLOG','2019-08-20 14:05:36'),(23,'5d5be28382c6a.png','FILE_USER_BLOG','2019-08-20 14:07:31'),(24,'5d5be2d9b0933.jpg','FILE_USER_BLOG','2019-08-20 14:08:57'),(25,'5d5be3177fd4c.png','FILE_USER_BLOG','2019-08-20 14:09:59'),(26,'5d5be37b5185f.png','FILE_USER_BLOG','2019-08-20 14:11:39'),(27,'5d5be3c6cc6db.png','FILE_USER_BLOG','2019-08-20 14:12:54'),(28,'5d5be4dfc9e61.png','FILE_USER_BLOG','2019-08-20 14:17:35'),(29,'5d5be514a88d8.png','FILE_USER_BLOG','2019-08-20 14:18:28'),(30,'5d5be55b385c6.jpg','FILE_USER_BLOG','2019-08-20 14:19:39'),(31,'5d5be624d0892.png','FILE_USER_BLOG','2019-08-20 14:23:00'),(32,'5d5be65b80369.jpg','FILE_USER_BLOG','2019-08-20 14:23:55'),(33,'5d5be6f6d7051.jpg','FILE_USER_BLOG','2019-08-20 14:26:30'),(34,'5d5be72750673.jpg','FILE_USER_BLOG','2019-08-20 14:27:19'),(35,'5d5be75ce3b25.png','FILE_USER_BLOG','2019-08-20 14:28:12'),(36,'5d5be78c27258.jpg','FILE_USER_BLOG','2019-08-20 14:29:00'),(37,'5d5be7a9ca083.jpg','FILE_USER_BLOG','2019-08-20 14:29:29'),(38,'5d5be7dfcee48.jpg','FILE_USER_BLOG','2019-08-20 14:30:23'),(39,'5d5bea7301c68.png','FILE_USER_BLOG','2019-08-20 14:41:23'),(40,'5d5bedcbadfa5.png','FILE_USER_BLOG_THUMBNAIL','2019-08-20 14:55:39'),(41,'5d5beedcf24e8.png','FILE_USER_BLOG_THUMBNAIL','2019-08-20 15:00:13'),(42,'5d5beef206b32.jpg','FILE_USER_BLOG_THUMBNAIL','2019-08-20 15:00:34'),(43,'5d5befa11a4a5.png','FILE_USER_BLOG_THUMBNAIL','2019-08-20 15:03:29'),(44,'5d5befb14dabc.jpg','FILE_USER_BLOG_THUMBNAIL','2019-08-20 15:03:45'),(45,'5d5d17eb61bfd.jpg','FILE_USER_BLOG','2019-08-21 12:07:39'),(46,'5d5d17f922786.jpg','FILE_USER_BLOG','2019-08-21 12:07:53'),(47,'5d5d1be78a442.jpg','FILE_USER_BLOG','2019-08-21 12:24:39'),(48,'5d5d1bf2dcd7f.jpg','FILE_USER_BLOG','2019-08-21 12:24:50'),(49,'5d5d20e299047.jpg','FILE_USER_BLOG','2019-08-21 12:45:54'),(50,'5d5d20f9e4ea1.jpg','FILE_USER_BLOG','2019-08-21 12:46:17'),(51,'5d5e40c6d7b9e.jpg','FILE_USER_BLOG','2019-08-22 09:14:14'),(52,'5d5e940d3531b.jpg','FILE_USER_BLOG','2019-08-22 15:09:33'),(53,'5d5e942554cff.jpg','FILE_USER_BLOG','2019-08-22 15:09:57'),(54,'5d5e944c0abff.png','FILE_USER_BLOG_THUMBNAIL','2019-08-22 15:10:36'),(55,'5d5e94d7e9e54.png','FILE_USER_BLOG','2019-08-22 15:12:55'),(56,'5d5e94e726392.jpg','FILE_USER_BLOG','2019-08-22 15:13:11'),(57,'5d5fd45c31f7d.png','FILE_USER_LOGO','2019-08-23 13:56:12'),(58,'5d65302e0a7a1.jpg','FILE_USER_BLOG','2019-08-27 15:29:18'),(59,'5d6534dd40a46.jpg','FILE_USER_BLOG','2019-08-27 15:49:17'),(60,'5d663d9426b13.png','FILE_USER_BLOG','2019-08-28 10:38:44'),(61,'5d665e1052893.jpg','FILE_USER_BLOG','2019-08-28 12:57:20'),(62,'5d665e518b5ea.jpg','FILE_USER_BLOG','2019-08-28 12:58:25'),(63,'5d665eaaf017f.jpg','FILE_USER_BLOG','2019-08-28 12:59:55'),(64,'5d665ed7b0e8d.jpg','FILE_USER_BLOG','2019-08-28 13:00:39'),(65,'5d66675bbda0a.jpg','FILE_USER_BLOG','2019-08-28 13:36:59'),(66,'5d667c6c83e78.jpg','FILE_USER_BLOG_THUMBNAIL','2019-08-28 15:06:52'),(67,'5d68d42d57529.jpg','FILE_USER_LOGO','2019-08-30 09:45:49'),(68,'5d68d517af558.jpg','FILE_USER_BLOG','2019-08-30 09:49:43'),(69,'5d6cb84cd0ca8.png','FILE_USER_BLOG','2019-09-02 08:35:56'),(70,'5d6cb8dd12a3e.jpg','FILE_USER_BLOG','2019-09-02 08:38:21'),(71,'5d6cbe920fe3f.png','FILE_USER_BLOG','2019-09-02 09:02:42'),(72,'5d6cbeedcca16.png','FILE_USER_BLOG','2019-09-02 09:04:13'),(73,'5d6cc00a71072.png','FILE_USER_BLOG','2019-09-02 09:08:58'),(74,'5d6cc051bbd9c.jpg','FILE_USER_BLOG','2019-09-02 09:10:09'),(75,'5d6cc10a8d9a4.jpg','FILE_USER_BLOG','2019-09-02 09:13:14'),(76,'5d6cc1fa8d7a6.jpg','FILE_USER_BLOG','2019-09-02 09:17:14');
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` (`version`) VALUES ('20190814133020'),('20190815111812'),('20190819080205'),('20190822084448'),('20190823083359'),('20190828104441'),('20190828122327');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-02 16:14:25
